
let PROVIDERS: any[] = [
  {
    provide: 'ApiEndpoint',
    useValue: ''
  },
  {
    provide: 'version', 
    useValue: '0.0.0'
  },
  // custom providers in production
];

export const environment = {
  production: true
};

export const ENV_PROVIDERS = [
  ...PROVIDERS
];
