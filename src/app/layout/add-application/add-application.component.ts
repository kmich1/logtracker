import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { ApplicationsService } from '../../../shared/api/rest/applications.service';
import { ErrorsService } from '../../shared/services/errors.service';
import { AlertService } from '../../shared/services/alert.service';
import { StorageService } from '../../../shared/api/services/storage.service';

import { ApplicationModel } from '../../../shared/models/application.model';

@Component({
    selector: 'app-add-application',
    templateUrl: './add-application.component.html',
    styleUrls: ['./add-application.component.scss'],
    animations: [routerTransition()]
})
export class AddApplicationComponent implements OnInit {

    application: ApplicationModel;

    toastInfo = {
        hidden: true,
        message: '',
        success: true
    }

    constructor(private _applicationsService: ApplicationsService,
                private _errorsService: ErrorsService,
                private _alertService: AlertService,
                private _storageService: StorageService) {
        this.application = new ApplicationModel('', '', '', '');
    }

    ngOnInit() {
    }

    public addApplication() {
        this._applicationsService.addApplication(this.application)
            .subscribe((res: any) => {
                this.toastInfo.success = true;
                this.toastInfo.message = res.name;
                this._storageService.saveApplicationsToSessionStorage(res);
            }, err => {
                this._errorsService.loginError(err)
                    .subscribe((error: any) => {
                        this._alertService.error(error.message);
                    });
                this.toastInfo.success = false;
                this.toastInfo.message = err._body;
            });
        this.showToast();
    }

    public selectType(event) {
        if (event.target.checked)
            this.application.type = event.target.value;
    }
    
    public trimName() {
        this.application.name = this.application.name.trim();
    }
    
    public trimDescription() {
        this.application.description = this.application.description.trim();
    }
    
    public trimLanguage() {
        this.application.language = this.application.language.trim();
    }

    public showToast() {
        this.toastInfo.hidden = false;

        setTimeout(() => {
            this.toastInfo.hidden = true;
            this.toastInfo.success = true;
            this.toastInfo.message = '';
        }, 10000);
    }
}
