import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-add-application-toast',
    templateUrl: './add-application-toast.component.html',
    styleUrls: ['./add-application-toast.component.scss']
})
export class AddApplicationToastComponent implements OnInit {
    @Input() success: boolean;
    @Input() message: string;

    constructor() {
    }

    ngOnInit() {
    }
}