import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { AddApplicationRoutingModule } from './add-application-routing.module';
import { AddApplicationComponent } from './add-application.component';
import { AddApplicationToastComponent } from './add-application-toast/add-application-toast.component';

@NgModule({
  imports: [
    CommonModule,
    AddApplicationRoutingModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    AddApplicationComponent,
    AddApplicationToastComponent
  ]
})
export class AddApplicationModule { }