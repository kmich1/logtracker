import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-application-card',
    templateUrl: './application-card.component.html',
    styleUrls: ['./application-card.component.scss']
})
export class ApplicationCardComponent implements OnInit {
    @Input() application;   

    constructor() {
    }

    ngOnInit() {
    }

    public cutDescription(text: string, chars: number) {
        if (text.length <= chars) 
            return text;
        else
            return text.slice(0, chars) + '...';
    }
}
