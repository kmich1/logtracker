import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { routerTransition } from '../../../router.animations';

import { ApplicationsService } from '../../../../shared/api/rest/applications.service';
import { ErrorsService } from '../../../shared/services/errors.service';
import { AlertService } from '../../../shared/services/alert.service';
import { StorageService } from '../../../../shared/api/services/storage.service';


@Component({
    selector: 'app-application-list',
    templateUrl: './application-list.component.html',
    styleUrls: ['./application-list.component.scss'],
    animations: [routerTransition()]
})
export class ApplicationListComponent implements OnInit, AfterContentChecked {

    applications = [];

    constructor(private _applicationsService: ApplicationsService,
                private _errorsService: ErrorsService,
                private _alertService: AlertService,
                private _storageService: StorageService) {
    }
    
    ngOnInit() {
        this.applications = this._storageService.getApplications();
    }
    
    ngAfterContentChecked() {
        this.applications = this._storageService.getApplications();
    }
}
