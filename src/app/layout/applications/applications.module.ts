import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { PageHeaderModule } from '../../shared/modules/page-header/page-header.module';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationCardComponent } from './application-list/application-card/application-card.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    NgbModule,
    TranslateModule,
    FormsModule,
    PageHeaderModule
  ],
  declarations: [
    ApplicationListComponent,
    ApplicationCardComponent,
    DashboardComponent
  ]
})
export class ApplicationsModule { }
