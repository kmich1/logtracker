import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgClass } from '@angular/common';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { Subscription } from 'rxjs/Subscription';

import { DocsService } from '../../../../shared/api/rest/docs.service';
import { StorageService } from '../../../../shared/api/services/storage.service';
import { ErrorsService } from '../../../shared/services/errors.service';
import { AlertService } from '../../../shared/services/alert.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit, OnDestroy {

    paramSub: Subscription;

    applicationName: string;
    applicationId: string;

    previousForm = {
        user: '',
        session: '',
        appVersion: ''
    };
    form = {
        user: '',
        session: '',
        appVersion: ''
    };
    
    beginModel: NgbDateStruct;
    beginDate: {
        year: number, 
        month: number
    };

    endModel: NgbDateStruct;
    endDate: {
        year: number, 
        month: number
    };

    type = '';
    limit = 100;
    page = 0;

    docs = [];

    selectedDoc: any;

    loading = false;

    constructor(private router: Router,
                private _activatedRoute: ActivatedRoute,
                private _docsService: DocsService,
                private _errorsService: ErrorsService,
                private _alertService: AlertService,
                private _storageService: StorageService) {
    }
    
    ngOnInit() {
        this.paramSub = this._activatedRoute.params.subscribe(params => {
            this.applicationName = params['name'];

            let app = this._storageService.getApplicationByName(this.applicationName);

            if (app) {
                this.applicationId = app._id;
                
                this.previousForm = {
                    user: '',
                    session: '',
                    appVersion: ''
                };
                this.form = {
                    user: '',
                    session: '',
                    appVersion: ''
                };

                this.beginModel = undefined;
                this.endModel = undefined;
                
                this.type = '';
                this.limit = 100;
                this.page = 0;
                
                this.getDocs(this.form);
            } else {
                this.router.navigate(['/applications/list']);
            }
        });
    }

    getDocs(_form: any) {
        this.loading = true;

        this.previousForm.user = _form.user;
        this.previousForm.session = _form.session;
        this.previousForm.appVersion = _form.appVersion;

        this.docs = [];

        let begin = this.beginModel 
            ? this.beginModel.year + '-' 
                + (this.beginModel.month < 10 ? '0' + this.beginModel.month : this.beginModel.month) + '-' 
                + (this.beginModel.day < 10 ? '0' + this.beginModel.day : this.beginModel.day)
            : undefined;
        let end = this.endModel
            ? this.endModel.year + '-' 
                + (this.endModel.month < 10 ? '0' + this.endModel.month : this.endModel.month) + '-' 
                + (this.endModel.day < 10 ? '0' + this.endModel.day : this.endModel.day)
            : undefined;

        let data = {
            limit: this.limit,
            page: this.page,
            begin: begin,
            end: end,
            user: _form.user,
            appVersion: _form.appVersion,
            session: _form.session,
            type: this.type
        }

        this._docsService.getDocs(this.applicationId, data)
            .subscribe((res: any) => {
                this.docs.push(...res);
                this.loading = false;
            }, err => {
                this._errorsService.loginError(err)
                    .subscribe((error: any) => {
                        this._alertService.error(error.message);
                        this.loading = false;
                    });
            });
    }

    clearFilters() {
        this.previousForm = {
            user: '',
            session: '',
            appVersion: ''
        };
        this.form = {
            user: '',
            session: '',
            appVersion: ''
        };

        this.beginModel = undefined;
        this.endModel = undefined;
        
        this.page = 0;

        this.getDocs(this.form);
    }
    
    validatePage() {
        if (this.page < 0) this.page = 0;
    }
    
    ngOnDestroy() {
        this.paramSub.unsubscribe();
    }
}
