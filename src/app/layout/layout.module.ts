import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent, SidebarComponent } from '../shared';

import { ApplicationsService } from '../../shared/api/rest/applications.service'
import { DocsService } from '../../shared/api/rest/docs.service'
import { AlertService } from '../shared/services/alert.service';
import { ErrorsService } from "../shared/services/errors.service";

@NgModule({
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        LayoutRoutingModule,
        NgbModule.forRoot(),
        TranslateModule
    ],
    declarations: [
        LayoutComponent,
        HeaderComponent,
        SidebarComponent
    ],
    providers: [
        ApplicationsService,
        DocsService,
        AlertService, 
        ErrorsService
    ]
})
export class LayoutModule { }
