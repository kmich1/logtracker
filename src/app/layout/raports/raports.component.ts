import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { StorageService } from '../../../shared/api/services/storage.service';

@Component({
    selector: 'raports',
    templateUrl: './raports.component.html',
    styleUrls: ['./raports.component.scss'],
    animations: [routerTransition()]
})
export class RaportsComponent implements OnInit {

    raports = [{
            name: 'lastVersionUsed',
            description: 'Last used application versions'
        }, {
            name: 'lastLoggedUsers',
            description: 'Users logged in in past 30 days'
    }];
    applications = [];

    form = {
        application: '',
        raport: ''
    }

    constructor(private _storageService: StorageService) {
        this.applications = this._storageService.getApplications();
    }

    ngOnInit() {
    }

    getAppId(appName: string): string {
        for (let app of this.applications) {
            if (app.name == appName)
                return app._id;
        }
        return '';
    }
}
