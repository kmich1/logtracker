import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { RaportsService } from '../../../shared/api/rest/raports.service'

import { RaportsRoutingModule } from './raports-routing.module';
import { RaportsComponent } from './raports.component';
import { LastVersionUsedComponent } from './last-version-used/last-version-used.component';
import { LastLoggedUsersComponent } from './last-logged-users/last-logged-users.component';

@NgModule({
  imports: [
    CommonModule,
    RaportsRoutingModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    RaportsComponent,
    LastVersionUsedComponent,
    LastLoggedUsersComponent
  ],
  providers: [
    RaportsService
  ]
})
export class RaportsModule { }