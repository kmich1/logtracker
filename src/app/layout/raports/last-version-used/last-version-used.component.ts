import { Component, OnInit, Input } from '@angular/core';
import { NgClass } from '@angular/common';
import { routerTransition } from '../../../router.animations';

import { Subscription } from 'rxjs/Subscription';

import { ErrorsService } from '../../../shared/services/errors.service';
import { AlertService } from '../../../shared/services/alert.service';
import { RaportsService } from '../../../../shared/api/rest/raports.service';

@Component({
    selector: 'app-last-version-used',
    templateUrl: './last-version-used.component.html',
    styleUrls: ['./last-version-used.component.scss'],
    animations: [routerTransition()]
})
export class LastVersionUsedComponent implements OnInit {
    
    loading = false;
    _application: string = '';
    records = [];

    get application(): string {
        return this._application;
    }

    @Input() set application(application: string) {
        this.loading = true;
        this._application = application;
        this.records = [];
        this._raportsService
            .getRaport(this.application, 'lastVersionUsed')
            .subscribe((res: any) => {
                this.records.push(...res);
                this.loading = false;
            }, err => {
                this._errorsService.loginError(err)
                    .subscribe((error: any) => {
                        this._alertService.error(error.message);
                        this.loading = false;
                    });
            });
    };

    constructor(private _errorsService: ErrorsService,
            private _alertService: AlertService,
            private _raportsService: RaportsService) {
    }
    
    ngOnInit() {
    }
}
