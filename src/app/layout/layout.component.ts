import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApplicationsService } from '../../shared/api/rest/applications.service';
import { ErrorsService } from '../shared/services/errors.service';
import { AlertService } from '../shared/services/alert.service';
import { StorageService } from '../../shared/api/services/storage.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(public router: Router,
                private _applications: ApplicationsService,
                private _errorsService: ErrorsService,
                private _alertService: AlertService,
                private _storageService: StorageService) { }

    ngOnInit() {
        this._storageService.deleteApplicationsfromSessionStorage();
        
        this._applications.getApplications()
            .subscribe((res: any) => {
                this._storageService.saveApplicationsToSessionStorage(...res);
            }, err => {
                this._errorsService.loginError(err)
                    .subscribe((error: any) => {
                        this._alertService.error(error.message);
                    });
            });

        if (this.router.url === '/') {
            this.router.navigate(['/applications']);
        }
    }

}
