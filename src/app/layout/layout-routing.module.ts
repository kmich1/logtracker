import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'applications', loadChildren: './applications/applications.module#ApplicationsModule' },
            { path: 'raports', loadChildren: './raports/raports.module#RaportsModule' },
            { path: 'add-application', loadChildren: './add-application/add-application.module#AddApplicationModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
