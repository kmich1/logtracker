import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, AbstractControl, FormBuilder, Validators } from "@angular/forms";

import { LoginService } from '../../shared/api/rest/login.service';
import { AlertService } from '../shared/services/alert.service';
import { StorageService } from "../../shared/api/services/storage.service";
import { ErrorsService } from "../shared/services/errors.service";

import { LoginModel } from '../../shared/models/login.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;
    public error: string;

    constructor(
      fb: FormBuilder,
      private router: Router,
      private _loginService: LoginService,
      private _alertService: AlertService,
      private _storage: StorageService,
      private _errorService: ErrorsService
    ) {
      this.form = fb.group({
        'username': ['', Validators.compose([Validators.required])],
        'password': ['', Validators.compose([Validators.required])]
      });
      this.email = this.form.controls['username'];
      this.password = this.form.controls['password'];
    }

    ngOnInit() {
    }

    onLogin(values: LoginModel) {
        if (this.form.valid) {
            this._loginService.login(values).subscribe((jwt:any) => {
                this._storage.setUserToSessionStorage(jwt);
                this.router.navigate(['/']);
            }, err => {
                this._errorService.loginError(err)
                    .subscribe((error: any) => {
                        this._alertService.error(error.message);
                    });
            });
        }
    }
    
    public trimSpacesLogin(){
        var val = this.form.controls['username'].value;
        this.form.patchValue({login: val.replace(/ /g,'')});
    }

    public trimSpacesPassword(){
        var val = this.form.controls['password'].value;
        this.form.patchValue({password: val.replace(/ /g,'')});
    }
}
