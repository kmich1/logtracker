import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import { LoginService } from '../../shared/api/rest/login.service';
import { AlertService } from '../shared/services/alert.service';
import { ErrorsService } from "../shared/services/errors.service";

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        TranslateModule,
        FormsModule, 
        ReactiveFormsModule
    ],
    providers: [
        LoginService,
        AlertService, 
        ErrorsService
    ],
    declarations: [LoginComponent]
})
export class LoginModule {
}
