import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { AuthService } from "../../../shared/api/services/auth.service";
import { YOUR_ACCES_TOKEN, YOUR_USER_NAME, YOUR_PERMISIONS } from "../../../shared/globals";
import { StorageService } from "../../../shared/api/services/storage.service";
import { Observable } from "rxjs";


@Injectable()
export class AuthGuardLayout implements CanActivate {


  constructor(private router: Router,
              private _authService: AuthService,
              private _storage: StorageService) {
  }


  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this._authService.isTokenInSessionStorage()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}