import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { AuthService } from "../../../shared/api/services/auth.service";
import { StorageService } from "../../../shared/api/services/storage.service";
import { Observable } from "rxjs";


@Injectable()
export class AuthGuardLogin implements CanActivate {

  constructor(private router: Router,
              private _authService: AuthService,
              private _storage: StorageService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this._authService.isTokenInSessionStorage()) {
      this.router.navigate(['']);
      return false;
    } else {
      return true;
    }
  }
}
