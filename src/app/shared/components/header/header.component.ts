import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../../../shared/api/services/auth.service';
import { StorageService } from '../../../../shared/api/services/storage.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    pushRightClass: string = 'push-right';
    username: string = 'User';
    
    constructor(private _translateService: TranslateService,
                public router: Router,
                private _authService: AuthService,
                private _storageService: StorageService) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.username = this._storageService.getUsername();
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    onLoggedOut() {
        this._authService.logoutUser();
        this.router.navigate(['/login']);
    }

    // changeLang(language: string) {
    //     this._translateService.use(language);
    // }
}
