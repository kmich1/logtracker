import { Component, OnInit, AfterContentChecked } from '@angular/core';

import { StorageService } from '../../../../shared/api/services/storage.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, AfterContentChecked {
    isActive = false;
    showMenu = '';
    applications = [];

    constructor(private _storageService: StorageService) {}
    
    ngOnInit() {
        this.applications = this._storageService.getApplications();
    }
        
    ngAfterContentChecked() {
        this.applications = this._storageService.getApplications();
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
}
