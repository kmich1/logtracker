import { Injectable } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { Observable } from "rxjs";

@Injectable()

export class ErrorsService{
  constructor(
    private translate: TranslateService
  ) {}

  getError(err) {
    return new Observable(observer => {
      if (!navigator.onLine) {
        this.translate.get('errors.connection').subscribe((message: string) => {
          observer.next({message: message, icon: 'settings_input_antenna'})
        });
      } else if (err.status == 409) {
        this.translate.get('errors.409').subscribe((message: string) => {
          observer.next({message: message})
        });
      } else if (err.status == 401) {
        this.translate.get('errors.401').subscribe((message: string) => {
          observer.next({message: message})
        });
      } else {
        this.translate.get('errors.server').subscribe((message: string) => {
          observer.next({message: message, icon: 'build'})
        });
      }
      observer.complete();
    });
  }

  loginError(err) {
    return new Observable(observer => {
      if (!navigator.onLine) {
        this.translate.get('errors.connection').subscribe((message: string) => {
          observer.next({message: message, icon: 'settings_input_antenna'})
        });
      } else if (err.status == 401) {
        this.translate.get('errors.login.401').subscribe((message: string) => {
          observer.next({message: message})
        });
      } else {
        this.translate.get('errors.server').subscribe((message: string) => {
          observer.next({message: message, icon: 'build'})
        });
      }
      observer.complete();
    });

  }

  loadedTradeEmptyError() {
    return new Observable(observer => {
      this.translate.get('errors.loadedTradeEmpty').subscribe((message: string) => {
          observer.next({message: message})
      });
        
      observer.complete();
    })
  }
}
