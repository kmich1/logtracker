import { Injectable, Inject } from "@angular/core";
import {
  YOUR_ACCES_TOKEN, YOUR_USER_NAME, YOUR_HEADER_NAME, PERMISSIONS, YOUR_PERMISIONS, YOUR_USER,
  YOUR_THRESHOLD
} from "../../globals";
import { RoleModel } from "../../../shared/models/role.model";

@Injectable()
export class StorageService {
  constructor() {
  }

  public havePermission(permArray: Array<string>) {
    let permissions = sessionStorage.getItem(YOUR_PERMISIONS);
    if (permissions) {
      for (let i in permArray) {
        if (permissions.indexOf(permArray[i]) === -1) {
          return false
        }
      }
      return true;
    }
    return false;
  }

  public getUsername() {
      return JSON.parse(sessionStorage.getItem(YOUR_USER)).username;
  }
  
  public getUserObject() {
      return JSON.parse(sessionStorage.getItem(YOUR_USER));
  }

  public getApplications() {
    return JSON.parse(sessionStorage.getItem("application_list"));
  }

  public getApplicationByName(name: string) {
    let result = undefined;

    let application_list = this.getApplications();
    if (!application_list || !application_list.length){
      return result;
    }
    
    for (let i = 0; i < application_list.length; i++) {
      if (application_list[i].name === name)
        result = application_list[i];
    }

    return result;
  }
  
  public setUserToSessionStorage(jwt: any) {
    sessionStorage.setItem(YOUR_USER_NAME, jwt.username);
    let permissions = [];
    jwt.roles.forEach((role: RoleModel) => {
      permissions.push(...role.permissions)
    });
    sessionStorage.setItem(YOUR_USER, JSON.stringify(jwt));
    sessionStorage.setItem(YOUR_ACCES_TOKEN, jwt.token);
    sessionStorage.setItem(YOUR_PERMISIONS, permissions.toString());
  }
  
  public saveTokenToLocalStorage(token: string) {
    localStorage.setItem(YOUR_ACCES_TOKEN, token);
  }
    
  public saveApplicationsToSessionStorage(...newApps) {
    let apps = sessionStorage.getItem("application_list")
      ? JSON.parse(sessionStorage.getItem("application_list"))
      : [];
    apps.push(...newApps);

    sessionStorage.setItem("application_list", JSON.stringify(apps));
  }

  public deleteApplicationsfromSessionStorage() {
    sessionStorage.setItem("application_list", JSON.stringify([]));
  }
}
