import { Injectable, Inject } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { YOUR_ACCES_TOKEN, YOUR_USER_NAME, YOUR_HEADER_NAME, YOUR_PERMISIONS, YOUR_USER } from "../../globals";

@Injectable()
export class AuthService {

  private JSON_HEADER: RequestOptions;

  constructor(private _http: Http, @Inject('ApiEndpoint') private apiEndpoint: string) {
  }

  // isTokenExpired(): boolean{
  //
  // }

  isTokenInSessionStorage(): boolean {
    return (null != sessionStorage.getItem(YOUR_ACCES_TOKEN));
  }

  tokenFromLocalStorage(): string {
    return localStorage.getItem(YOUR_ACCES_TOKEN);
  }

  clearTokenFromLocalStorage(): void {
    localStorage.removeItem(YOUR_ACCES_TOKEN);
  }

  logoutUser() {
    localStorage.removeItem(YOUR_ACCES_TOKEN);
    sessionStorage.removeItem(YOUR_ACCES_TOKEN);
    sessionStorage.removeItem(YOUR_USER_NAME);
    sessionStorage.removeItem(YOUR_PERMISIONS);
    sessionStorage.removeItem(YOUR_USER);
  }

  refreshToken(token: string) {
    let headers = new Headers();
    headers.append('authorization', token);
    this.JSON_HEADER = new RequestOptions({ headers: headers });
    return this._http.post(`${this.apiEndpoint}/auth/refreshToken`, {} , this.JSON_HEADER)
      .map((res:any) => {
        let result = res.json();
        result.token = res.headers.get('authorization');
        return result;
      });
  }
}
