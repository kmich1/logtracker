import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";

import { StorageService } from '../services/storage.service';

@Injectable()
export class RaportsService {

  constructor(private _http: Http, 
              private _storage: StorageService,
              @Inject('ApiEndpoint') private apiEndpoint: string) {
  }
  
  getRaport(appId: string, raport: string) {
    let userObject = this._storage.getUserObject();
    let token: string = userObject ? userObject.token : '';

    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);

    return this._http.get(`${this.apiEndpoint}/api/raports/${appId}?name=${raport}`, { headers: headers })
      .map((res:any) => {
        let result = res.json();
        return result;
    });
  }
}
