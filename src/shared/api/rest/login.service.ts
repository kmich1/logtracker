import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { LoginModel } from "../../models/login.model";

@Injectable()
export class LoginService {

  private JSON_HEADER = { headers: new Headers({'Content-Type': 'application/json'}) };

  constructor(private _http: Http, @Inject('ApiEndpoint') private apiEndpoint: string) {
  }

  login(body: LoginModel) {
    return this._http.post(`${this.apiEndpoint}/auth/login`,
      body, this.JSON_HEADER).map((res:any) => {
      let result = res.json();
      result.token = res.headers.get('authorization');
      return result;
    });
  }
}
