import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";

import { StorageService } from '../services/storage.service';

@Injectable()
export class DocsService {

  constructor(private _http: Http, 
              private _storage: StorageService,
              @Inject('ApiEndpoint') private apiEndpoint: string) {
  }
  
  getDocs(app: string, data: any) {
    let userObject = this._storage.getUserObject();
    let token: string = userObject ? userObject.token : '';

    let headers: Headers = new Headers();
    headers.append('Authorization', token);

    let query = '';
    if (data.limit) query += 'limit=' + data.limit;
    else query += 'limit=100';
    if (data.page) query += '&page=' + data.page;
    else query += '&page=0';
    if (data.begin) query += '&begin=' + data.begin;
    if (data.end) query += '&end=' + data.end;
    if (data.user) query += '&user=' + data.user;
    if (data.appVersion) query += '&appVersion=' + data.appVersion;
    if (data.session) query += '&session=' + data.session;
    if (data.type) query += '&type=' + data.type;

    return this._http.get(`${this.apiEndpoint}/api/logrecords/${app}?${query}`, { headers: headers })
      .map((res:any) => {
        let result = res.json();
        return result;
    });
  }
}
