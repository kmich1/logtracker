export class ApplicationModel {
    constructor(
        public type: string,
        public name: string,
        public description: string,
        public language: string
    ) {}
  }