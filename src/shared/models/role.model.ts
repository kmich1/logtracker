export class RoleModel {
    constructor(
        public name:string,
        public description: string,
        public permissions: Array<string>
    ) {}
}